import numpy as np
import cv2
import os
import time
import argparse

"""
The two ascii "color" schemes below for 70 and 10 levels of gray are taken from the book
*Python Playground: Geeky Projects for the Curious Programmer* by *Mahesh Venkitachalam*.
"""

# 70 levels of gray
gscale1 = '$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/|()1{}[]?-_+~<>i!lI;:,\"^`. '
# 10 levels of gray
gscale2 = '@%#*+=-:. '

gray_mappings = {70: gscale1, 10: gscale2}


def image_to_ascii(im, width=200, mapping=10, font_ratio=0.40, invert=False):
    height = width * im.shape[0] / im.shape[1]

    # Correct height with font aspect ratio
    height = round(height * font_ratio)

    scaled = cv2.resize(im, (width, height))
    gray = cv2.cvtColor(scaled, cv2.COLOR_BGR2GRAY)/255

    mapping_str = gray_mappings[mapping]

    if invert:
        mapping_str = mapping_str[::-1]

    mapped = (gray * len(mapping_str)).astype(int)

    ascii = np.take(np.array(list(mapping_str)), mapped)
    lines = [''.join(l) for l in ascii]

    return lines


def video_to_ascii(f, fps=30, **kwargs):
    cap = cv2.VideoCapture(f)
    sleep_time = 1/fps

    while True:
        ret, frame = cap.read()

        if not ret:
            break

        yield image_to_ascii(frame, **kwargs)
        time.sleep(sleep_time)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('file', type=str, help='Video file to convert to ascii')
    parser.add_argument('--width', type=int, default=100, help='Width of ascii art (number of characters)')
    parser.add_argument('--mapping', type=int, default=10, choices=[10, 70], help='Number of color levels')
    parser.add_argument('--fps', type=int, default=20, help='Refresh rate')
    parser.add_argument('--invert', default=False, action='store_true', help='Inverted color scheme')

    args = parser.parse_args()

    for frame in video_to_ascii(args.file, fps=args.fps, width=args.width, mapping=args.mapping, invert=args.invert):
        os.system('clear')
        [print(line) for line in frame]


if __name__ == '__main__':
    main()
