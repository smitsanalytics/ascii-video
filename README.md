# ascii-video

Converts videos to ascii art

This script is based on the idea to convert images to ascii art from the book
*Python Playground: Geeky Projects for the Curious Programmer* by *Mahesh Venkitachalam*.
The two ascii "color schemes" were taken from this book.